import React, { Component } from 'react';
import logo from '../images/logo.svg';
import './App.css';
import CommentBox from '../components/CommentBox'

class App extends Component {
  render() {
    return (
      <div className="App">
        <section>
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <p className="App-intro"></p>
          <div id="story-app"></div>
        </section>
        <div id="comment-box">
          <CommentBox />;
        </div>
      </div>
    );
  }
}

export default App;
