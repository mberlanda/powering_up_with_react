import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
// import StoryBoard from './components/StoryBoard';
// import CommentBox from './components/CommentBox';
// import { Router, Route, browserHistory} from 'react-router';
import { BrowserRouter, Route } from 'react-router-dom'

const app = (
    <BrowserRouter>
        <Route path="/" component={App} />
    </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
