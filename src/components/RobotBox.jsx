import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// challenge 1.5 Creating a Component

const pi = Math.PI; // challenge 1.10
const topics = ["React", "JSX", "JavaScript", "Programming"]; // challenge 1.11
class RobotBox extends Component {
    render() {
        return (

            // challenge 1.5 <div>Hello From React</div>
            // challenge 1.9 Coding JSX
            <div>
                <h3>McCircuit is my name</h3>
                <p className="message">I am here to help.</p>
                {/* challenge 1.10 JSX and Plain JavaScript */}
                <div className="is-tasty-pie" >
                    The value of PI is approximately {pi}
                </div>
                {/* challenge 1.11 JSX and Collections */}
                <div>
                    <h3>Topics I am interested in</h3>
                    <ul>
                        {topics.map(t => <li>{t.toString()}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

// challenge 1.6 Rendering a Component
let target = document.getElementById('robot-app');
ReactDOM.render(<RobotBox />, target);
