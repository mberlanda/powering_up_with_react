import React, {Component} from 'react';

class Comment extends Component {
    constructor() {
        super();
        this.state = {
            isAbusive: false
        };
    }
    render(){
        let commentBody;
        if (!this.state.isAbusive) {
            commentBody = this.props.body;
        } else {
            commentBody = <em>Content marked as abusive</em>;
        }
        return (
            <div className="comment">
                <img src={this.props.avatarUrl} alt={`${this.props.author}'s picture`} />
                <p className="comment-header">
                    {this.props.author}
                </p>
                <p className="comment-body">
                    {commentBody}
                </p>
                <p className="comment-footer">
                    <a href="#" className="comment-footer-delete" onClick={this._handleDelete.bind(this)}>
                        Delete Comment
                    </a>
                    <a href="#" onClick={this._toggleAbuse.bind(this)}>Report as Abuse</a>
                </p>
            </div>
        );
    }

    _handleDelete(event){
        event.preventDefault();
        if (window.confirm('Are you sure?')){
            this.props.onDelete(this.props.comment);
        }
    }

    _toggleAbuse(event){
        event.preventDefault();
        this.setState({
            isAbusive: !this.state.isAbusive
        });
    }
}

export default Comment;