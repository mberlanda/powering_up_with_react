import React, { Component } from 'react';

class ComponentForm extends Component {
    render(){
        return(
            <form className="comment-form" onSubmit={this._handleSubmit.bind(this)}>
                <label>Join the discussion</label>
                <div className="comment-form-fields">
                    <input placeholder="Name:" ref={(input) => this._author = input}/>
                    <textarea
                        placeholder="Comment:"
                        ref={(textarea) => this._body = textarea}
                        onKeyUp ={ this._getCharacterCount.bind(this) }
                    ></textarea>
                </div>
                <div className="comment-form-actions">
                    <button type="Submit">
                        Post comment
                    </button>
                </div>
            </form>
        )
    }

    _handleSubmit(event){
        event.preventDefault();
        let author = this._author;
        let body = this._body;

        if (!this._author.value || !this._body.value) {
            alert("Please enter your name and comment");
            return
        }

        this.props.addComment(author.value, body.value);
    }

    _getCharacterCount() {
        this.setState({
            characters: this._body.value.length
        })
    }
}

export default ComponentForm;