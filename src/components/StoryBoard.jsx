import React, { Component } from 'react';

const now = new Date();
const topicsList = ['HTML', 'JavaScript', 'React'];

class StoryBoard extends Component {
    render(){
        return(
            <div>
                <h3> Story Box </h3>
                <p className="lead">Current Time: {now.toDateString()}</p>
                <ul>
                    {topicsList.map( topic => <li>{topic}</li> )}
                </ul>
            </div>
        );
    }
}
export default StoryBoard;