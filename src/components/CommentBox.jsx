import React, {Component} from 'react';
import jQuery from 'jquery';
import Comment from './Comment';
import CommentForm from './CommentForm';
import CommentAvatarList from './CommentAvatarList';

class CommentBox extends Component {
    constructor() {
        super();
        this.state = {
            showComments: false,
            comments: []
        };
    }
    componentWillMount() {
        this._fetchComments();
    }
    render(){
        const comments = this._getComments() || [];
        let commentNodes;
        let buttonText = 'Show comments';
        if (this.state.showComments) {
            buttonText = 'Hide comments'
            commentNodes = <div className="comment-list">{comments}</div>;
        }
        return(
            <div className="comment-box">
                <CommentForm addComment={this._addComment.bind(this)} />
                <CommentAvatarList avatars={this._getAvatars()} />
                <h3>Comments</h3>
                {this._getPopularMessage(comments.length)}
                <h4 className="comment-count">
                    {this._getCommentsTitle(comments.length)}
                </h4>
                <button onClick={this._handleClick.bind(this)}>{buttonText}</button>
                {commentNodes}
            </div>
        );
    }

    componentDidMount(){
        this._timer = setInterval(
            () => this._fetchComments(),
            15000
        );
    }

    componentWillUnmount(){
        clearInterval(this._timer);
    }

    _addComment(author, body){
        const comment = {
            /* should be assigned by the remote server */
            id: this.state.comments.length + 1,
            author,
            body,
            avatarUrl: `https://robohash.org/${author}.png`
        };
        jQuery.post('/api/comments', { comment })
            .done(newComment => {
                this.setState({ comments: this.state.comments.concat([newComment]) });
            })
            .always(()=>{
                this.setState({ comments: this.state.comments.concat([comment]) });
            })
    }

    _deleteComment(comment){
        jQuery.ajax({
            mehtod: 'DELETE',
            url: `/api/comments/${comment.id}`
        });

        const comments = [...this.state.comments];
        const commentIndex = comments.indexOf(comment);
        comments.splice(commentIndex, 1);

        this.setState({ comments });
    }

    _fetchComments(){
        jQuery.ajax({
            method: 'GET',
            url: '/api/comments.json',
            success: (data) => {
                const comments = data.comments;
                this.setState({ comments });
            }
        });
    }

    _getComments() {
        return this.state.comments.map(c =>
            <Comment
                comment={c}
                key={c.id}
                author={c.author}
                body={c.body}
                avatarUrl={c.avatarUrl}
                onDelete={this._deleteComment.bind(this)}
            />
        );
    }

    _getCommentsTitle(commentCount) {
        if (commentCount === 0) {
            return 'No comments yet';
        } else if(commentCount === 1) {
            return '1 comment';
        } else {
            return `${commentCount} comments`;
        }
    }

    _getAvatars(){
        return this.state.comments.map((comment) => comment.avatarUrl)
    }

    _getPopularMessage(commentCount){
        const POPULAR_COUNT = 10;
        if (commentCount > POPULAR_COUNT) {
            return (
                <div>
                    This post is getting really popular, don't miss out!
                </div>
            );
        }
    }

    _handleClick(){
        this.setState({
            showComments:!this.state.showComments
        });
    }
}

export default CommentBox;