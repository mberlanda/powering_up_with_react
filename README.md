# Powering up with React

This repo contains the materials and the exercises from Powering up with React by CodeSchool.

## Getting started

https://yarnpkg.com/lang/en/docs/cli/create/

```
yarn global add create-react-app
~/.yarn/bin/create-react-app <project-directory>
```